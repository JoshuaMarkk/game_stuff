﻿using UnityEngine;

namespace Assets.Scripts.Utilities
{
	public enum MouseState
	{
		Down,
		Held,
		Up
	}
}
