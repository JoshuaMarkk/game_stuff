﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
	public static class Game
	{
		public static Vector2 GetMousePosition()
		{
			Vector3 pos = CameraManager.Instance.Camera.ScreenToWorldPoint(Input.mousePosition);
			pos.z = 0f;
			return pos;
		}
	}
}
