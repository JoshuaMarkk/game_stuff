﻿using UnityEngine;

public class BetterBehaviour : MonoBehaviour
{
	protected string Format(string log, object[] replaces)
	{
		string soFar = "";
		int replace = 0;
		bool canReplace = true;

		foreach (char c in log)
		{
			string thisStr = "" + c;

			if (c == '!')
			{
				canReplace = false;
			}
			else
			{
				if (c == '%' && canReplace)
				{
					thisStr = "" + replaces[replace];
					replace += 1;
				}

				canReplace = true;
			}

			soFar += thisStr;
		}

		return soFar.Replace("!%", "%");
	}

	protected void Log(string log, params object[] replaces)
	{
		Debug.Log(Format(log, replaces));
	}

	protected void LogError(string log, params object[] replaces)
	{
		Debug.LogError(Format(log, replaces));
	}

	protected void LogWarning(string log, params object[] replaces)
	{
		Debug.LogWarning(Format(log, replaces));
	}
}
