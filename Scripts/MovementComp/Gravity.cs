﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.InputWrapperComp;

namespace Assets.Scripts.MovementComp
{
	public class Gravity : MonoBehaviour
	{
		[SerializeField] private float gravSpeed;

		private Movement mov;

		private void Awake()
		{
			mov = GetComponent<Movement> ();
		}

		private void Update()
		{
			mov.AddForce (Vector2.down * gravSpeed);
		}
	}
}
