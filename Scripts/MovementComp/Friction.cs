﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.InputWrapperComp;

namespace Assets.Scripts.MovementComp
{
	public class Friction : MonoBehaviour {

		private Movement mov;
		[SerializeField] private float friction;

		void Awake()
		{
			mov = GetComponent<Movement> ();
		}

		void Update()
		{
			mov.AddForce (new Vector2(-mov.MoveDir.x * friction,0));
		}

	}
}