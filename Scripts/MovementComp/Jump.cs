﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.InputWrapperComp;
using Assets.Scripts.Collision;

namespace Assets.Scripts.MovementComp
{
	public class Jump : BetterBehaviour
	{
		[SerializeField] private float jumpSpeed;

		private Movement mov;
		private RaycastCollider2D rcd;

		private void OnEnable() 
		{
			GetComponent<InputWrapper>().OnJump += OnJump;
		}

		private void Awake()
		{
			rcd = GetComponent<RaycastCollider2D> ();
			mov = GetComponent<Movement> ();
		}

		private void OnJump()
		{
			if (rcd.BottomBlocked)
			{
				mov.AddForce (Vector2.up * jumpSpeed);
			}
		}
	}
}