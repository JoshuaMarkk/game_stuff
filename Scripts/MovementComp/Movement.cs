﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.Scripts.MovementComp
{
	public class Movement : MonoBehaviour {

		public Vector2 MoveDir { get; set; }

		public event Action OnBeforeMove;
		public event Action OnAfterMove;

		public void AddForce(Vector2 v)
		{
			MoveDir += v;
		}

		void Update()
		{
			if (OnBeforeMove != null)
			{
				OnBeforeMove();
			}

			transform.position += (Vector3) MoveDir * Time.deltaTime;

			if (OnAfterMove != null)
			{
				OnAfterMove();
			}
		}

	}
}
