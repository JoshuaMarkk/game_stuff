﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.InputWrapperComp;

namespace Assets.Scripts.MovementComp
{
	public class ControlX : MonoBehaviour {

		private Movement mov;
		[SerializeField] private float speed;

		void OnEnable() 
		{
			GetComponent<InputWrapper>().OnMove += OnMove;
		}

		void Awake()
		{
			mov = GetComponent<Movement> ();
		}
			
		private void OnMove(Vector2 direction)
		{
			mov.AddForce (direction * speed);

		}
	}
}
