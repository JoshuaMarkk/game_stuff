﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.MovementComp;
using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.Collision
{
	public class RaycastCollider2D : BetterBehaviour
	{
		private Movement movement;
		private BoxCollider2D coll;
		private float epsilon = 0.25f;

		private float xMax;
		private float yMax;
		private float xMin;
		private float yMin;

		private Vector2 Left
		{
			get
			{
				Vector2 ret = (Vector2)transform.position + Vector2.right * -coll.size.x / 2;
				ret.x += epsilon;
				return ret;
			}
		}

		private Vector2 Right
		{
			get
			{
				Vector2 ret = (Vector2)transform.position + Vector2.right * coll.size.x / 2;
				ret.x -= epsilon;
				return ret;
			}
		}

		private Vector2 Up
		{
			get
			{
				Vector2 ret = (Vector2)transform.position + Vector2.up * coll.size.x / 2;
				ret.y -= epsilon;
				return ret;
			}
		}

		private Vector2 Down
		{
			get
			{
				Vector2 ret = (Vector2)transform.position + Vector2.up * -coll.size.x / 2;
				ret.y += epsilon;
				return ret;
			}
		}

		public bool TopBlocked
		{
			get
			{
				return transform.position.y >= yMax;
			}
		}

		public bool BottomBlocked
		{
			get
			{
				return transform.position.y <= yMin;
			}
		}

		public bool RightBlocked
		{
			get
			{
				return transform.position.x >= xMax;
			}
		}

		public bool LeftBlocked
		{
			get
			{
				return transform.position.x <= xMin;
			}
		}

		private List<RaycastHit2D> DoubleCast(Vector2 posOne, Vector2 dirOne, Vector2 posTwo, Vector2 dirTwo, float distance)
		{
			List<RaycastHit2D> hits = Physics2D.RaycastAll(posOne, dirOne, distance).ToList();
			hits.AddRange(Physics2D.RaycastAll(posTwo, dirTwo, distance));
			return hits;
		}

		private Vector2 GetClosestHit(List<RaycastHit2D> hits, float defaultDistance)
		{
			Vector2 closestXMax = new Vector2(defaultDistance, defaultDistance);
			float closestDistance = Mathf.Infinity;

			foreach (RaycastHit2D hit in hits)
			{
				Vector2 point = hit.point;
				float dist = Vector2.Distance(transform.position, point);

				if (dist < closestDistance)
				{
					closestDistance = dist;
					closestXMax = point;
				}
            }

			return closestXMax;
		}

		private void FindMaxes()
		{
			List<RaycastHit2D> xMaxHits = DoubleCast(Up, Vector2.right, Down, Vector2.right, Mathf.Infinity).FindAll(h => h.collider.tag == "solid");
			List<RaycastHit2D> xMinHits = DoubleCast(Up, Vector2.left, Down, Vector2.left, Mathf.Infinity).FindAll(h => h.collider.tag == "solid");
			List<RaycastHit2D> yMaxHits = DoubleCast(Left, Vector2.up, Right, Vector2.up, Mathf.Infinity).FindAll(h => h.collider.tag == "solid");
			List<RaycastHit2D> yMinHits = DoubleCast(Left, Vector2.down, Right, Vector2.down, Mathf.Infinity).FindAll(h => h.collider.tag == "solid");

			xMax = GetClosestHit(xMaxHits, Mathf.Infinity).x - coll.size.x / 2;
			xMin = GetClosestHit(xMinHits, -Mathf.Infinity).x + coll.size.x / 2;
			yMax = GetClosestHit(yMaxHits, Mathf.Infinity).y - coll.size.y / 2;
			yMin = GetClosestHit(yMinHits, -Mathf.Infinity).y + coll.size.y / 2;
		}

		private void CheckMaxes()
		{
			Vector3 newPosition = transform.position;
			Vector2 newMoveDir = movement.MoveDir;

			if (TopBlocked)
			{
				newPosition.y = yMax;
				if(newMoveDir.y > 0) newMoveDir.y = 0;
			}

			if (RightBlocked)
			{
				newPosition.x = xMax;
				if(newMoveDir.x > 0) newMoveDir.x = 0;
			}

			if (BottomBlocked)
			{
				newPosition.y = yMin;
				if(newMoveDir.y < 0) newMoveDir.y = 0;
			}

			if (LeftBlocked)
			{
				newPosition.x = xMin;
				if(newMoveDir.x < 0) newMoveDir.x = 0;
			}

			movement.MoveDir = newMoveDir;
			transform.position = newPosition;
		}

		private void OnEnable()
		{
			movement.OnBeforeMove += FindMaxes;
			movement.OnAfterMove += CheckMaxes;
		}

		private void Awake()
		{
			movement = GetComponent<Movement>();
			coll = GetComponent<BoxCollider2D>();
		}
	}
}
