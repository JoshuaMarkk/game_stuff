﻿using System;
using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.InputComponents
{
	public class InputWrapper : BetterBehaviour
	{
		public event Action<Vector2> OnMove;
		public event Action OnJump;
		public event Action<Vector2, MouseState> OnShoot;
		public event Action<int> OnSwitchWeapon;

		protected void RaiseOnMove(Vector2 direction)
		{
			if (OnMove != null)
			{
				OnMove(direction);
			}
		}

		protected void RaiseOnJump()
		{
			if (OnJump != null)
			{
				OnJump();
			}
		}

		protected void RaiseOnShoot(Vector2 mousePosition, MouseState state)
		{
			if (OnShoot != null)
			{
				OnShoot(mousePosition, state);
			}
		}

		protected void RaiseOnSwitchWeapon(int direction)
		{
			if (OnSwitchWeapon != null)
			{
				OnSwitchWeapon(direction);
			}
		}
	}
}
