﻿using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.InputWrapperComp
{
	public class InputPlayer : InputWrapper
	{
		Vector2 moveDirection;

		private void Update()
		{
			moveDirection.x = 0;
            if (Input.GetKey("a"))
			{
				moveDirection.x = -1;
			}
			else if (Input.GetKey("d"))
			{
				moveDirection.x = 1;
			}

			RaiseOnMove(moveDirection);

			if (Input.GetKey(KeyCode.Space))
			{
				RaiseOnJump();
			}

			if (Input.GetMouseButtonDown(0))
			{
				RaiseOnShoot(Game.GetMousePosition(), MouseState.Down);
			}
			else if (Input.GetMouseButton(0))
			{
				RaiseOnShoot(Game.GetMousePosition(), MouseState.Held);
			}
			else if (Input.GetMouseButtonUp(0))
			{
				RaiseOnShoot(Game.GetMousePosition(), MouseState.Up);
			}

			if (Input.GetKeyDown("q"))
			{
				RaiseOnSwitchWeapon(-1);
			}
			else if (Input.GetKeyDown("e"))
			{
				RaiseOnSwitchWeapon(1);
			}
		}
	}
}
