﻿using Assets.Scripts.Utilities;
using UnityEngine;

namespace Assets.Scripts.Managers
{
	public class CameraManager : BetterBehaviour
	{
		public static CameraManager Instance { get; private set; }

		[SerializeField] private Camera cam;

		public Camera Camera
		{
			get { return cam; }
		}

		private void Awake()
		{
			Instance = this;
		}
	}
}
